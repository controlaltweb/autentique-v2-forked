<?php

namespace vinicinbgs\Autentique;

use vinicinbgs\Autentique\Enums\ResourcesEnum;
use vinicinbgs\Autentique\Utils\Api;

class BaseResource
{
    protected $api;

    protected $sandbox;

    protected $resourcesEnum;

    public function __construct(bool $sandbox)
    {
        $this->api = new Api("https://api.autentique.com.br/v2/graphql");

        $this->sandbox = (string) $sandbox;

        $this->resourcesEnum = ResourcesEnum::class;
    }
}
